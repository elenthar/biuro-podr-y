package com.mwal.test_maven.entities;

import javax.persistence.*;

@Entity
public class Photo {
	@Id
	@GeneratedValue
	private long id;
	
	private String name;
	private String URL;
	
	public Photo(){}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getURL() {
		return URL;
	}

	public void setURL(String uRL) {
		URL = uRL;
	}


}
