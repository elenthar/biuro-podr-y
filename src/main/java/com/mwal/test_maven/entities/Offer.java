package com.mwal.test_maven.entities;

import java.util.Date;

import javax.persistence.*;

@Entity
@NamedQueries({
	@NamedQuery(name = "Offer.findAll", query = "SELECT o from Offer o"),
	@NamedQuery(name = "Offer.findById", query = "SELECT o from Offer o WHERE o.id = :id"),
	@NamedQuery(name = "Offer.findByDates", query = "SELECT o from Offer o where o.startDate between :startDate and :endDate")
})
public class Offer {
	
	@Id
	@GeneratedValue
	private long id;
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	private String title;
	private double price;
	private Date startDate;
	@Transient
	private String asText;
	
	public String getAsText() {
		return toString();
	}
	private Date endDate;
	@Lob
	private String shortDescription;
	@Lob
	private String longDescription;
	
	private String photoURL;
	
	public String getPhotoURL() {
		return photoURL;
	}
	public void setPhotoURL(String photoURL) {
		this.photoURL = photoURL;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public double getPrice() {
		return price;
	}
	public void setPrice(double price) {
		this.price = price;
	}
	public Date getStartDate() {
		return startDate;
	}
	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}
	public Date getEndDate() {
		return endDate;
	}
	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public String getShortDescription() {
		return shortDescription;
	}
	public void setShortDescription(String shortDescription) {
		this.shortDescription = shortDescription;
	}
	public String getLongDescription() {
		return longDescription;
	}
	public void setLongDescription(String longDescription) {
		this.longDescription = longDescription;
	}
	
	@Override
	public String toString(){
		return getTitle() + " " + getShortDescription();
	}
	

}
