package com.mwal.test_maven.controller;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.persistence.EntityManager;

import com.mwal.test_maven.config.DBManager;
import com.mwal.test_maven.entities.Offer;

@ManagedBean
@SessionScoped
public class OfferBean implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Offer offer = new Offer();
	
	private String title;
	private double price;
	private Date startDate;
	private Date endDate;
	private String shortDescription;
	private String longDescription;
	private String photoURL;
	
	
	public String getPhotoURL() {
		return photoURL;
	}

	public void setPhotoURL(String photoURL) {
		this.photoURL = photoURL;
	}

	public String addOffer(){
		offer.setTitle(title);
		System.out.println("Entered addOffer");
		System.out.println("title = " + title);
		offer.setShortDescription(shortDescription);
		offer.setLongDescription(longDescription);
		offer.setPhotoURL(photoURL);
		offer.setStartDate(startDate);
		offer.setEndDate(endDate);
		DBManager manager = DBManager.getManager();
		EntityManager entityMan = manager.createEntityManager();
		entityMan.getTransaction().begin();
		entityMan.persist(offer);
		entityMan.getTransaction().commit();
		this.offer = new Offer();
		return "index?faces-redirect=true";
	}

	private byte[] photo;
	
	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public double getPrice() {
		return price;
	}
	
	public List<Offer> getOfferList(){
		EntityManager manager = DBManager.getManager().createEntityManager();
		List<Offer> list = manager.createNamedQuery("Offer.findAll").getResultList();
		System.out.println("List length: " + list.size());
		manager.close();
		return list;		
	}
	
	public void listener(ActionEvent event){
		FacesContext context = FacesContext.getCurrentInstance();
		ExternalContext exCtx = context.getExternalContext();
		Map<String, String> params = exCtx.getRequestParameterMap();		
		String idString = params.get("id").toString();
		long id = Long.parseLong(idString);
		EntityManager manager = DBManager.getManager().createEntityManager();
		Offer result = (Offer) manager.createNamedQuery("Offer.findById").setParameter("id", id).getResultList().get(0);
		setOffer(result);
	}
	
	public String singleOffer(){
		
		return "offerDetails?faces-redirect=true";
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public String getShortDescription() {
		return shortDescription;
	}

	public void setShortDescription(String shortDescription) {
		this.shortDescription = shortDescription;
	}

	public String getLongDescription() {
		return longDescription;
	}

	public void setLongDescription(String longDescription) {
		this.longDescription = longDescription;
	}

	public byte[] getPhoto() {
		return photo;
	}

	public void setPhoto(byte[] photo) {
		this.photo = photo;
	}

	public OfferBean(){
		
	}

	public Offer getOffer() {
		return offer;
	}

	public void setOffer(Offer offer) {
		this.offer = offer;
	}
	
	
}
