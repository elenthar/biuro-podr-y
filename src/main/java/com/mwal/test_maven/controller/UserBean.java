package com.mwal.test_maven.controller;

import java.util.Date;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.persistence.EntityManager;

import com.mwal.test_maven.config.DBManager;
import com.mwal.test_maven.entities.User;

@ManagedBean
@SessionScoped
public class UserBean {
	
	private User user= new User();
	private long id;
	
	private String userName;

	private String name;
	private String surname;
	private String email;
	private String pesel;
	private Date birthday;
	private String country;
	
	
	
	public String addUser(){
		user.setAdmin(false);
		user.setBirthday(birthday);
		user.setCountry(country);
		user.setEmail(email);
		user.setName(name);
		user.setPesel(pesel);
		user.setSurname(surname);
		user.setUserName(userName);
		DBManager manager = DBManager.getManager();
		EntityManager entityMan = manager.createEntityManager();
		entityMan.getTransaction().begin();
		entityMan.persist(user);
		entityMan.getTransaction().commit();
		this.user = new User();
		return "index?faces-redirect=true";
	}
	
	
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getSurname() {
		return surname;
	}
	public void setSurname(String surname) {
		this.surname = surname;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPesel() {
		return pesel;
	}
	public void setPesel(String pesel) {
		this.pesel = pesel;
	}
	public Date getBirthday() {
		return birthday;
	}
	public void setBirthday(Date birthday) {
		this.birthday = birthday;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public boolean isAdmin() {
		return isAdmin;
	}
	public void setAdmin(boolean isAdmin) {
		this.isAdmin = isAdmin;
	}
	private boolean isAdmin;

}
